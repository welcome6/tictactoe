/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sosad.tictactoe;

import java.util.Scanner;

/**
 *
 * @author Bunny0_
 */
public class TicTacToe {
    public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int row, column;
		char player = 'X';

		// create 2 dimensional array for tic tac toe board
		char[][] board = new char[3][3];
		char ch = '1';
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				board[i][j] = '-';
			}
		}
		System.out.println("Welcome to OX Game");
                displayBoard(board);
                while (winner(board) == 0) {

			// get input for row/column
			System.out.println(player + " turn");
			System.out.println("Please input Row Col :");
			row = in.nextInt() - 1;
			column = in.nextInt() - 1;

			// occupied
			if (board[row][column] == 'X' || board[row][column] == 'O') {				
				System.out.println("This spot is occupied. Please try again");
				continue;
			}
			// place the X
			board[row][column] = player;
			displayBoard(board);

			if (winner(board) != 0) {
				System.out.println("Player " + player + " win.....");
			}

			//swap players 
			if (player == 'O') {
				player = 'X';

			} else {
				player = 'O';
			}
			if (winner(board) == 0 && !hasFreeSpace(board)) {
				System.out.println("The game is a draw. Please try again.");
				break;
			}
		}

		
		in.close();
		System.out.println("Bye bye...");
		
	}

	public static void displayBoard(char[][] board) {
		System.out.println("  1 2 3");
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				if (j == 0) {
					System.out.print(i + 1 + " ");
				}
				System.out.print(board[i][j] + " ");
			}
			System.out.println("");
		}
	}
        //Winning conditions in all forms
	public static char winner(char[][] board) {
		for (int i = 0; i < 3; i++) {
			if (board[i][0] == board[i][1] && board[i][0] == board[i][2] && board[i][0] != '-') {
				return board[i][0];

			}
			if (board[0][i] == board[1][i] && board[0][i] == board[2][i] && board[0][i] != '-') {
				return board[0][i];
			}
			if (board[0][0] == board[1][1] && board[0][0] == board[2][2] && board[0][1] != '-') {
				return board[0][0];

			}
			if (board[2][0] == board[1][1] && board[2][0] == board[0][2] && board[2][0] != '-') {
				return board[2][0];
			}
		}
		return 0;
        }
        //Check for empty values in the board
	public static boolean hasFreeSpace(char[][] board) {
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				if (board[i][j] != 'O' && board[i][j] != 'X') {
					return true;
				}
			}
		}
		return false;
        }
}
